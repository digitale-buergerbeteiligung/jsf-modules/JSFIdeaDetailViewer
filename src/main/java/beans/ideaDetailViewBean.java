package beans;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.primefaces.context.RequestContext;

import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import ideaService.model.Ideas;
import ideaService.service.IdeasLocalService;
import ideaService.service.VideosLocalService;
import ideasService.service.enums.ReviewStatus;
import ideasService.service.enums.VideoExtensions;
import servicetrackers.IdeaLocalServiceTracker;
import servicetrackers.LayoutLocalServiceTracker;
import servicetrackers.UserLocalServiceTracker;
import servicetrackers.VideosLocalServiceTracker;

@ManagedBean
@SessionScoped
public class ideaDetailViewBean implements Serializable{

	/**
	 *
	 */
	private static final long serialVersionUID = -971105890695619804L;

	private IdeaLocalServiceTracker ideasLocalServiceTracker;
	private LayoutLocalServiceTracker layoutLocalServiceTracker;
	private UserLocalServiceTracker userLocalServiceTracker;
	private VideosLocalServiceTracker videosLocalServiceTracker;
	private Ideas idea;
	private static Logger logger;
	private int votes;
	private List<String> images;

    public List<String> getImages() {
        return images;
    }

	public String oggUrl;
	public String webmUrl;
	public String mp4Url;

	public String pitch;

	private String openGallery = "null";



	/**
	 * @return the openGallery
	 */
	public String getOpenGallery() {
		return openGallery;
	}


	/**
	 * @param openGallery the openGallery to set
	 */
	public void setOpenGallery(String openGallery) {
		this.openGallery = openGallery;
	}


	@PostConstruct
    public void postConstruct() {
        Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        BundleContext bundleContext = bundle.getBundleContext();
        ideasLocalServiceTracker = new IdeaLocalServiceTracker(bundleContext);
        layoutLocalServiceTracker = new  LayoutLocalServiceTracker(bundleContext);
        userLocalServiceTracker = new UserLocalServiceTracker(bundleContext);
        videosLocalServiceTracker = new VideosLocalServiceTracker(bundleContext);
        userLocalServiceTracker.open();
        ideasLocalServiceTracker.open();
        layoutLocalServiceTracker.open();
        videosLocalServiceTracker.open();
		logger = LoggerFactory.getLogger(this.getClass().getName());
		logger.info(this.getClass().getName() +" initialized successfully");
    }


    @PreDestroy
    public void preDestroy() {
    	ideasLocalServiceTracker.close();
    	layoutLocalServiceTracker.close();
    	userLocalServiceTracker.close();
    	videosLocalServiceTracker.close();
    }

    public void onPageLoad(){
    	getIdeasForCurrentLayout();
    }
    
    public boolean isLoggedIn() {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		if (!themeDisplay.isSignedIn()) {
//			try {
//				externalContext.redirect("/c/portal/login");
//			} catch (IOException e) {
//				logger.error("No redirect possible" , e);
//			}
			return false;
		}else{
			
			return true;
		}				
	}
	
	public boolean showVoteButton() {
		if(this.isLoggedIn() && this.isIdeaInValidPeriod() && this.userCanVote())
			return true;
		
		if(!this.isLoggedIn() && this.isIdeaInValidPeriod())
			return true;
		
		return false;
	}
    
	private boolean userCanVote() {
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext()
				.getRequestMap().get(WebKeys.THEME_DISPLAY);
		if (themeDisplay.isSignedIn()) {
			IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
			try {
				long ideaId = idea.getPrimaryKey();
				logger.info(Long.toString(ideaId));
				User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
				String[] voterUsernames = ideasLocalService.getIdeas(ideaId).getRating().split(",");
				for (String username : voterUsernames) {
					// user has already voted for this idea
					if (username.equals(user.getScreenName())) {
						return false;
					}
				}				
			} catch (Exception e) {
				logger.info(e + " --------------- ideaDetailViewBean::userCanVote -------------");				
			}
			return true;
		} else {
			return false;
		}
	}
	
    private boolean isIdeaInValidPeriod() {
    	long ideaId = idea.getPrimaryKey();
		logger.info(Long.toString(ideaId));
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		try {
			// get current date			 
			 Date currentDate = new Date();
			
			//shift current date to the prev month (also changes year if month is JANUARY)
			Calendar calCurrent = Calendar.getInstance();
			calCurrent.setTime(currentDate);
			calCurrent.add(Calendar.MONTH, -1);
			currentDate = calCurrent.getTime();			
			
			//get current year
			int currentYear = this.getYearOfDate(currentDate);
			logger.info("Current year = " + Integer.toString(currentYear));

			//get current month
			int currentMonth = this.getMonthOfDate(currentDate);
			logger.info("Current Month = " + currentMonth);

			Date createDateOfIdea = ideasLocalService.getIdeas(ideaId).getCreateDate();
			
			//shift createDate of idea to previous month
			Calendar calIdea = Calendar.getInstance();
			calIdea.setTime(createDateOfIdea);
			calIdea.add(Calendar.MONTH, -1);
			createDateOfIdea = calIdea.getTime();
			
			//get year of the idea
			int yearOfIdea = this.getYearOfDate(createDateOfIdea);
			logger.info("Year of Idea = " + Integer.toString(yearOfIdea));
						
			//get month of idea in number and not in string
			int monthOfIdea = this.getMonthOfDate(createDateOfIdea);
			logger.info("Month of Idea = " + Integer.toString(monthOfIdea));
			
			int[] ideaPeriod = this.getPeriod(monthOfIdea);
			logger.info("Idea period = " + ideaPeriod[0] + " " + ideaPeriod[1]);
			
			int[] currentPeriod = this.getPeriod(currentMonth);
			logger.info("Current period = " + currentPeriod[0] + " " + currentPeriod[1]);

			//if the idea belongs to the current year and in the current period, then it's available for voting
			if(yearOfIdea == currentYear && this.samePeriods(ideaPeriod, currentPeriod)) {
				logger.info(" ===== IDEA IS VALID FOR VOTING ====");
				return true;
			}
		} catch (Exception e) {
			logger.info(" ===== isIdeaInValidPeriod ====");
			e.printStackTrace();
		}
		
		logger.info(" ===== IDEA IS ***NOT*** VALID FOR VOTING ====");
		return false;
	}
    
	private int getYearOfDate(Date date) {		
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int yearOfIdea = cal.get(Calendar.YEAR);
		return yearOfIdea;
	}
	
	private int getMonthOfDate(Date date) {
		LocalDate localIdeaDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		int monthOfIdea = localIdeaDate.getMonthValue();
		return monthOfIdea;
	}
    
	/**
	 * Return the period of the given month. The months of the year are split by two, defining one period.
	 * @param month The month to be tested
	 * @return The period that the month belongs to
	 */
	private int[] getPeriod(int month) {
		Map<String,int[]> monthPeriods = new HashMap<String, int[]>();
		monthPeriods.put("JAN_FEB", new int[]{1,2});
		monthPeriods.put("MAR_APR", new int[]{3,4});
		monthPeriods.put("MAY_JUN", new int[]{5,6});
		monthPeriods.put("JUL_AUG", new int[]{7,8});
		monthPeriods.put("SEPT_OCT", new int[]{9,10});
		monthPeriods.put("NOV_DEC", new int[]{11,12});
		
		int[] period = new int[]{};
		switch (month) {
			case 1: 
				period = monthPeriods.get("JAN_FEB");
				break;
			case 2: 
				period = monthPeriods.get("JAN_FEB");
				break;
			case 3: 
				period = monthPeriods.get("MAR_APR");
				break;
			case 4: 
				period = monthPeriods.get("MAR_APR");
				break;
			case 5: 
				period = monthPeriods.get("MAY_JUN");
				break;
			case 6: 
				period = monthPeriods.get("MAY_JUN");
				break;
			case 7: 
				period = monthPeriods.get("JUL_AUG");
				break;
		   case 8:  
			   period = monthPeriods.get("JUL_AUG");
			   break;
		   case 9:  
			   period = monthPeriods.get("SEPT_OCT");
			   break;
		   case 10: 
			   period = monthPeriods.get("SEPT_OCT");
			   break;
		   case 11: 
			   period = monthPeriods.get("NOV_DEC");
			   break;
		   case 12: 
			   period = monthPeriods.get("NOV_DEC");
			   break;
		   default:
			   break;
		}
		return period;
	}
	
	/**
	 * Compare if the months of the two periods are the same.
	 * @return true if periods are the same, false otherwise
	 */
	private boolean samePeriods(int[] period1, int[] period2) {
		if(period1[0] == period2[0] && period1[1] == period2[1]) {
			logger.info("Idea period and current period MATCH");
			return true;
		}
		logger.info("Idea period and current period ***DON'T*** MATCH");
		return false;
	}
    
    public void likeIdea (){
    	if(!isLoggedIn()) {
			logger.info("------- User not logged in ------- ");
			FacesContext context = FacesContext.getCurrentInstance();
			ExternalContext externalContext = context.getExternalContext();
			try {
				externalContext.redirect("/c/portal/login");
			} catch (Exception e1) {
				logger.error("could not redirect to login page", e1);
			}
		} else {
			IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
			try {
				User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
				ideasLocalService.addUserToRating(user , idea.getPrimaryKey());
				RequestContext.getCurrentInstance().update("_JSFIdeaDetailViewer_WAR_JSFIdeaDetailViewer_:ideaDetailViewForm:voteCount");
				logger.info(user.getScreenName() + " has liked idea with primary key " +idea.getPrimaryKey());
			} catch (Exception e) {
				FacesContext context = FacesContext.getCurrentInstance();
		    	ExternalContext externalContext = context.getExternalContext();
		    	try {
					externalContext.redirect("/c/portal/login");
				} catch (Exception e1) {
					logger.error("could not redirect to login page", e1);
				}
			}
		}
    }

    public int getVotes(){
    	IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
    	this.votes =  ideasLocalService.getIdeasRatingCount(idea.getPrimaryKey());
    	return this.votes;
    }

	private User getCurrentUser(String id) throws NumberFormatException, PortalException{
		UserLocalService userLocalService = userLocalServiceTracker.getService();
		return userLocalService.getUserById(Long.parseLong(id));
	}

    private void getIdeasForCurrentLayout(){
    	IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
    	ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);
    	long currentLayoutId = themeDisplay.getLayout().getPrimaryKey();
    	Ideas i = ideasLocalService.getIdeasByLayoutIdRef(currentLayoutId);
    	this.pitch = i.getPitch();

    	VideosLocalService videosService = videosLocalServiceTracker.getService();

    	mp4Url = videosService.getVideoUrlByIdeaRefAndExtension(i.getPrimaryKey(), VideoExtensions.MP4);
    	oggUrl = videosService.getVideoUrlByIdeaRefAndExtension(i.getPrimaryKey(), VideoExtensions.OGV);
    	webmUrl = videosService.getVideoUrlByIdeaRefAndExtension(i.getPrimaryKey(), VideoExtensions.WEBM);

        images = new ArrayList<String>();

        if(!ideasLocalService.getPictureUrlByIdeasRefAndPosition(i.getPrimaryKey(), 0).equals("/"))
        	images.add(ideasLocalService.getPictureUrlByIdeasRefAndPosition(i.getPrimaryKey(), 0));
        if(!ideasLocalService.getPictureUrlByIdeasRefAndPosition(i.getPrimaryKey(), 1).equals("/"))
	        images.add(ideasLocalService.getPictureUrlByIdeasRefAndPosition(i.getPrimaryKey(), 1));
        if(!ideasLocalService.getPictureUrlByIdeasRefAndPosition(i.getPrimaryKey(), 2).equals("/"))
	        images.add(ideasLocalService.getPictureUrlByIdeasRefAndPosition(i.getPrimaryKey(), 2));

        if( mp4Url.equals("/")){
        	this.openGallery = "0";
        }else{
        	this.openGallery = "null";
        }

    	this.setIdea(i);
    }

    public boolean showVideoPlayer(){
    	if(mp4Url != "" && mp4Url != null &&  !mp4Url.equals("/")){
    		return true;
    	}
    	return false;
    }

    public boolean showPitch(){
    	if(this.pitch != null && this.pitch != ""){
    		return true;
    	}
    	return false;
    }

    public String getPageUrl(){
    	ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);
    	return themeDisplay.getURLPortal()+ themeDisplay.getURLCurrent() ;
    }

    public boolean showShareButton(){
	    	try {
				if(this.getIdea().getReviewStatus().equals(ReviewStatus.ACCEPTED.getReviewStatusDescription())){
					return true;
				}
			} catch (Exception e) {
				return false;
			}
	    	return false;
    }

    public boolean showImages(){
    	if(this.images.size() >= 1 ){
    		return true;
    	}
    	return false;
    }




	/**
	 * @return the idea
	 */
	public Ideas getIdea() {
		return idea;
	}

	/**
	 * @param idea the idea to set
	 */
	public void setIdea(Ideas idea) {
		this.idea = idea;
	}


	/**
	 * @return the oggUrl
	 */
	public String getOggUrl() {
		return oggUrl;
	}


	/**
	 * @param oggUrl the oggUrl to set
	 */
	public void setOggUrl(String oggUrl) {
		this.oggUrl = oggUrl;
	}


	/**
	 * @return the webmUrl
	 */
	public String getWebmUrl() {
		return webmUrl;
	}


	/**
	 * @param webmUrl the webmUrl to set
	 */
	public void setWebmUrl(String webmUrl) {
		this.webmUrl = webmUrl;
	}


	/**
	 * @return the mp4Url
	 */
	public String getMp4Url() {
		return mp4Url;
	}


	/**
	 * @param mp4Url the mp4Url to set
	 */
	public void setMp4Url(String mp4Url) {
		this.mp4Url = mp4Url;
	}

    /**
	 * @return the pitch
	 */
	public String getPitch() {
		return pitch;
	}


	/**
	 * @param pitch the pitch to set
	 */
	public void setPitch(String pitch) {
		this.pitch = pitch;
	}

}
